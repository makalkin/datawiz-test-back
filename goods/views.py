from django.shortcuts import render
from dwapi import datawiz
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
import datetime
import pandas as pd
import math
# Create your views here.
def sales(request):
    dw = datawiz.DW(request.META['HTTP_API_KEY'], request.META['HTTP_API_SECRET']) 
    client = dw.get_client_info()
    shops = list(client['shops'].keys())

    data = dw.get_receipts(shops=shops, date_from=datetime.date(2015, 11, 17), date_to=datetime.date(2015, 11, 18))

    df = pd.DataFrame(data)

    df.date = df.date.dt.strftime('%Y-%m-%d')

    grouped = df.groupby('date')

    res = grouped.agg({ 
        'receipt_id': 'count', 
        'turnover': 'sum', 
        'cartitems': lambda x: pd.DataFrame([item for sublist in list(x) for item in sublist] )['total_price'].mean()
    })

    res['Items Count'] = grouped.agg(lambda x: pd.DataFrame([item for sublist in list(x) for item in sublist] )['qty'].sum())
    res.rename(columns={'receipt_id': 'Checks Count', 'cartitems': 'Check Average', 'turnover': 'Turnover' }, inplace=True)

    res = res.T
    res['diff %'] = (res.iloc[:, 1] - res.iloc[:, 0]) / res.iloc[:, 0]
    res['diff'] = res.iloc[:, 1] - res.iloc[:, 0]

    return JsonResponse({ 'data': res.to_dict('split') })

def goods(request):
    page = int(request.GET.get('page', 0))
    per_page = int(request.GET.get('per_page', 10))

    dw = datawiz.DW(request.META['HTTP_API_KEY'], request.META['HTTP_API_SECRET']) 
    client = dw.get_client_info()
    shops = list(client['shops'].keys())
    data = dw.get_receipts(shops=shops, date_from=datetime.date(2015, 11, 17), date_to=datetime.date(2015, 11, 18))

    df = pd.DataFrame(data)
    df.date = df.date.dt.strftime('%Y-%m-%d')

    items_by_date = df.groupby(['date'], as_index=False)

    items = items_by_date.agg({
        'cartitems': lambda x: pd
            .DataFrame([item for sublist in list(x) for item in sublist] )
            .groupby('product_id', as_index=False)
            .agg({'qty': 'sum', 'total_price': 'sum'}) 
    })

    day_1 = items.iloc[0, 1]
    day_2 =  items.iloc[1, 1]
    
    res = day_1.merge(day_2, on='product_id', how='outer' ).fillna(0)
    res['Quantity Difference'] = res['qty_x'] - res['qty_y']
    res['Turnover Difference'] = res['total_price_x'] - res['total_price_y']
    
    pages = math.ceil(len(res) / per_page)
    res = res.sort_values('Turnover Difference')[page * per_page: (page + 1) * per_page]

    products = list(res['product_id'])
    products = pd.DataFrame(dw.get_product(products=products))

    res = res.merge(products, on='product_id', how='inner')
    
    res.rename(columns={'product_name': 'Product Name'}, inplace=True)
    res = res[['Product Name', 'Quantity Difference', 'Turnover Difference']]

    return JsonResponse({
        'data': res.to_dict('split'),
        'pages': pages,
        'per_page': per_page,
        'page': page
    })


def user(request):
    dw = datawiz.DW(request.META['HTTP_API_KEY'], request.META['HTTP_API_SECRET']) 
    data = dw.get_client_info()
    return JsonResponse(data)


@csrf_exempt
def signin(request):
    data = json.loads(request.body.decode('utf-8'))
    print(data)
    email = data['email']
    password = data['password']

    dw = datawiz.Auth()


    return JsonResponse(dw.generate_secret(email, password))